from flask import request, jsonify
from flask_api import FlaskAPI
from instance.config import app_config
from urllib.error import URLError, HTTPError
from urllib.request import urlopen
from bs4 import BeautifulSoup

import re


def create_app(config_name):
    app = FlaskAPI(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')

    @app.route('/', methods=['POST'])
    def hipchat_client():

        message_string = str(request.data.get('message_string', ''))

        # Check for 3 different input types, no order required

        # Mentions
        mentions = extract_mentions(message_string)

        # Emoticons
        emoticons = extract_emoticons(message_string)

        # Links
        links = extract_links(message_string)

        data = {}

        # Separating this for clarity
        if mentions:
            data['mentions'] = mentions

        if emoticons:
            data['emoticons'] = emoticons

        if links:
            data['links'] = links

        response = jsonify(data)
        response.status_code = 200

        return response

    def extract_mentions(message_string):
        mentions = sorted(re.findall(r'(?<=@)[\w]*(?=\W)', message_string))
        return mentions

    def extract_emoticons(message_string):
        return sorted(re.findall(r'(?<=\()[\w]{1,15}(?=\))', message_string))

    def extract_links(message_string):
        urls = sorted(re.findall(r'(https?://[^\s]+)', message_string))

        links = []

        for url in urls:
            try:
                # No guarantees about the url being valid
                page = urlopen(url)
            except HTTPError as e:
                print('Error code: ', e.code)
            except URLError as e:
                print('Reason: ', e.reason)
            else:
                # We should be good! Only want to add to our list if everything checks out
                title = BeautifulSoup(page, 'html.parser').title.string
                links.append({
                    'url': url,
                    'title': title
                })

        return links

    return app
