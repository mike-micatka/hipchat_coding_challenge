from unittest import TestCase
from nose.tools import nottest
from app import create_app
import json


class HipChatClientTestCase(TestCase):

    def setUp(self):
        self.app = create_app(config_name='testing')
        self.client = self.app.test_client

    @nottest
    def test_builder(self, message_string, expected_result):
        result = self.client().post('/', data={
            "message_string": message_string
        })

        data = json.loads(result.get_data(as_text=True))

        self.assertEqual(result.status_code, 200)
        self.assertEqual(expected_result, data)

    def test_no_result(self):
        message_string = 'hello world. I am a boring string'

        # Should be an empty dictionary as a return
        expected_result = {}

        self.test_builder(message_string, expected_result)

    def test_single_mention(self):
        message_string = '@buster where is your hand?'

        expected_result = {
            'mentions': [
                'buster'
            ]
        }

        self.test_builder(message_string, expected_result)

    def test_multiple_mentions(self):
        message_string = '@george_michael @maeby who is working the banana stand?'

        expected_result = {
            'mentions': [
                'maeby',
                'george_michael'
            ]
        }

        expected_result['mentions'].sort()

        self.test_builder(message_string, expected_result)

    def test_messy_mention(self):
        message_string = 'how much was that suit @gob?'

        expected_result = {
            'mentions': [
                'gob'
            ]
        }

        self.test_builder(message_string, expected_result)

    def test_multiple_emoticon(self):
        message_string = '(magic_trick) vs (illusion)'

        expected_result = {
            'emoticons': [
                'magic_trick',
                'illusion'
            ]
        }

        expected_result['emoticons'].sort()

        self.test_builder(message_string, expected_result)

    def test_long_emoticon(self):
        message_string = '(thisemoticonistoolongtobevalid)'

        # Should be an empty dictionary as a return
        expected_result = {}

        self.test_builder(message_string, expected_result)

    def test_single_url(self):
        message_string = 'Not sure what I expected http://gifstelevision.tumblr.com/post/53471854823'

        expected_result = {
            'links': [
                {
                    'url': 'http://gifstelevision.tumblr.com/post/53471854823',
                    'title': 'TELEVISION GIFS'
                }
            ]
        }

        self.test_builder(message_string, expected_result)

    def test_multiple_urls(self):
        message_string = 'http://i.imgur.com/RbnRBLk.gifv http://gph.is/29MsGZT'

        # Our order here is by url, that is enforced on the backend
        expected_result = {
            'links': [
                {
                    'url': 'http://gph.is/29MsGZT',
                    'title': 'Arrested Development GIFs - Find & Share on GIPHY'
                },
                {
                    'url': 'http://i.imgur.com/RbnRBLk.gifv',
                    'title': 'The entire first week after I buy a new pair of shoes'
                },

            ]
        }

        self.test_builder(message_string, expected_result)

    def test_invalid_url(self):
        # This is the only test that really has an "invalid" input option, we have no way of validating
        # emoticons or mentions (at this time)

        # this probably will not change but cannot guarantee (please don't buy this domain to make me look bad)
        message_string = 'http://garbageurl.com'

        expected_result = {}

        self.test_builder(message_string, expected_result)

    def test_combination(self):
        message_string = '@steve_holt (steve_holt) https://www.youtube.com/watch?v=rREGbLdOzfg'

        expected_result = {
            'mentions': [
                'steve_holt'
            ],

            'links': [
                {
                    'url': 'https://www.youtube.com/watch?v=rREGbLdOzfg',
                    'title': 'Steve Holt! - YouTube'
                }
            ],

            'emoticons': [
                'steve_holt'
            ]
        }

        self.test_builder(message_string, expected_result)
