# Hipchat Code Challenge

## Project Requirements

Please write a RESTful API that takes a chat message string as input and returns a JSON object containing information about its contents as described below.

Your service should parse the following data from the input:
1. mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character. (https://confluence.atlassian.com/hipchat/get-teammates-attention-744328217.html)
2. Emoticons - For this exercise, you only need to consider 'custom' emoticons which are alphanumeric strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon. (https://www.hipchat.com/emoticons)
3. Links - Any URLs contained in the message, along with the page's title.

The response should be a JSON object containing arrays of all matches parsed from the input string.
For example, calling your function with the following inputs should result in the corresponding return values.

## Examples

Input: 

```"@chris you around?"```

Return:
```json
{
  "mentions": [
    "chris"
  ]
}
```

Input: 

```"Good morning! (megusta) (coffee)"```

Return:
```json
{
  "emoticons": [
    "megusta",
    "coffee"
  ]
}
```

Input: 

```"Olympics are starting soon; http://www.nbcolympics.com"```

Return:
```json
{
  "links": [
    {
      "url": "http://www.nbcolympics.com",
      "title": "2016 Rio Olympic Games | NBC Olympics"
    }
  ]
}
```
Input:

```"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"```

Return:
```json
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ],
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
    }
  ]
}
```

## Getting the environment setup

In order to keep a consistent development/testing environment 
we are going to be using a virtual environment. Install according to your OS and run as follows:

1. ```virtualenv -p python3 venv```
1. ```source .env```
1. ```pip3 install -r requirements.txt```

## Running tests
1. ```nosetests```

## Using the API

1. ```flask run```
1. Make POST requests to the IP shown by the above command in the following format
    * {message_string: 'your message here'}
    
## Sources

1. https://scotch.io/tutorials/build-a-restful-api-with-flask-the-tdd-way
